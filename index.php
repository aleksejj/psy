<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

    <title>Ассоциация психоаналитического коучинга и бизнес-консультирования / articul club</title>
    <meta name="description" content="" />
    <meta name="keywords" content="" />

    <meta property="og:image" content="https://psy.articul.ru/local/templates/psy/images/og-image.jpg" />
    <meta property="og:image:width" content="1200" />
    <meta property="og:image:height" content="837" />

    <meta name="theme-color" content="#ff7326" />
    <link rel="icon" href="/favicon.ico" />
    <link rel="manifest" href="/manifest.json" />

    <?
      $entryPointsFile = realpath($_SERVER['DOCUMENT_ROOT'] . '/local/templates/psy/assets' . '/entrypoints.json');

      if (!file_exists($entryPointsFile)) {
        return;
      }

      $file_contents = file_get_contents($entryPointsFile);
      $entrypoints = json_decode($file_contents, true);

      $html = '';
      
      foreach ($entrypoints as $file) {
        foreach ($file['js'] as $js) {
          $html .= '<script src="' . $js . '" defer ></script>';
        }
        foreach ($file['css'] as $css) {
          $html .= '<link href="' . $css . '" rel="stylesheet"/>';
        }
      }

      echo $html;
    ?>
  </head>
  <body id="app" class="noselect" ondragstart="return false;" ondrop="return false;">
    <? include_once $_SERVER['DOCUMENT_ROOT'] . '/local/templates/psy/header.php' ?>

    <main>
        <? include_once $_SERVER['DOCUMENT_ROOT'] . '/local/templates/psy/include/first-banner.php' ?>
        <? include_once $_SERVER['DOCUMENT_ROOT'] . '/local/templates/psy/include/second-banner.php' ?>
        <? include_once $_SERVER['DOCUMENT_ROOT'] . '/local/templates/psy/include/third-banner.php' ?>
        <?/* include_once $_SERVER['DOCUMENT_ROOT'] . '/local/templates/psy/include/form.php' */?>
        <? include_once $_SERVER['DOCUMENT_ROOT'] . '/local/templates/psy/include/contacts.php' ?>
    </main>

    <? include_once $_SERVER['DOCUMENT_ROOT'] . '/local/templates/psy/footer.php' ?>
  </body>
</html>
