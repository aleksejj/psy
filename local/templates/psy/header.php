<header class="page-header">
  <div class="page-header__container">
    <a href="http://apcbc.ru" target="_blank" class="svg-icon svg-icon--logo">
      <svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#logo"></use></svg>
    </a>
  </div>
</header>
