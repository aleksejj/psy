import axios from 'axios';
import { validateEmail } from '../../utils/validateEmail';

const mainForm = document.querySelectorAll('[data-main-form]');
const mainFormEndPoint = '/';

const formSubmitter = (form) => {
  let errorsCounter = null;
  
  const {
    adult,
    email,
    fio,
    personal,
    phone,
    time,
    submitter
  } = form.elements;
  
  const fieldsArrayView = [adult, email, fio, personal, phone, time];
  
  const submitHandler = (event) => {
    event.preventDefault();

    if (errorsCounter > 0) {
      return;
    }

    errorsCounter = 0;

    if (!fio.value) {
      fio.classList.add('error');
      errorsCounter += 1;
    }
  
    if (!validateEmail(email.value) || !email.value) {
      email.classList.add('error');
      errorsCounter += 1;
    }
  
    if (phone.value.length < 18) {
      phone.classList.add('error');
      errorsCounter += 1;
    }
  
    // time.classList.add('error');
  
    if (!personal.checked) {
      personal.classList.add('error');
      errorsCounter += 1;
    }
  
    if (!adult.checked) {
      adult.classList.add('error');
      errorsCounter += 1;
    }
  
    if (errorsCounter !== 0) {
      submitter.disabled = true;
      return;
    }

    const fd = new FormData;
    const obj = {
      adult, email, fio, personal, phone, time
    };
    
    for (let key of Object.keys(obj)) {
      fd.append(key, obj[key].value);
    }

    axios({
      method: "post",
      url: mainFormEndPoint,
      data: fd,
      // headers: { "Content-Type": "multipart/form-data" }
    })
      .then(function (response) {
        console.log(response);
      })
      .catch(function (response) {
        console.log(response);
      });
  }

  const removeError = ({currentTarget}) => {
    if (!currentTarget.classList.contains('error')) {
      return;
    }
    
    currentTarget.classList.remove('error');
    errorsCounter -= 1;
    
    if (errorsCounter === 0) {
      submitter.disabled = false;
    }
  }

  fieldsArrayView.forEach(el => {
    el.addEventListener('click', removeError);
  });

  form.addEventListener('submit', submitHandler);
};

mainForm.forEach(el => {
  formSubmitter(el);
})
