!function () {
  const observer = new IntersectionObserver(entries => {
    entries.forEach(el => {
      if (!el.isIntersecting) {
        return;
      }

      if (!el.target.dataset.hasOwnProperty('lazySrc')) {
        return;
      }
    
      el.target.src = el.target.dataset.lazySrc;
      el.target.removeAttribute('data-lazy-src');
    });
  }, {
    root: document.getElementById('#layout'),
    rootMargin: '0px',
    threshold: .5
  });
  
  document.querySelectorAll('img').forEach(el => {
    observer.observe(el);
  })
}();
