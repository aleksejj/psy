<form class="main-form" data-main-form novalidate autocomplete="off">
  <div class="main-form__inner">
      <h2 class="main-form__label">Анкета</h2>

    <div class="main-form__fieldsets">
      <div>
        <fieldset class="main-form__fieldset">
          <input
            class="main-form__input base-input"
            type="text"
            name="fio"
            placeholder="ФИО*"
          >

          <div class="main-form__fields-wrapper">
            <input
              class="main-form__input base-input"
              type="tel"
              name="phone"
              placeholder="Мобильный телефон*"
              data-phone-mask
            >

            <input
              class="main-form__input base-input"
              type="text" name="time"
              placeholder="Время консультации"
              maxlength="30"
            >
          </div>

          <input
            class="main-form__input base-input"
            type="email"
            name="email"
            placeholder="E-mail*"
          >
        </fieldset>

        <fieldset class="main-form__fieldset main-form__fieldset--desktop-flex">
          <label class="main-form__checkbox base-checkbox">
            Я подтверждаю, что я старше 18-ти лет *
            <input type="checkbox" name="adult" value="Y">
            <span class="base-checkbox__checkmark"></span>
          </label>

          <label class="main-form__checkbox base-checkbox">
            Я даю свое согласие на обработку персональных данных *
            <input type="checkbox" name="personal" value="Y">
            <span class="base-checkbox__checkmark"></span>
          </label>
        </fieldset>
      </div>

      <button class="main-form__submit base-button" name="submitter">
        Отправить
      </button>
    </div>
    </div>
</form>

