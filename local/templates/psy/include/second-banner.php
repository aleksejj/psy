<div class="second-banner">
  <div class="second-banner__inner">
    <div class="second-banner__heading">
      <span class="second-banner__heading-fw700">как лучше</span>
      <span class="second-banner__heading-fw200">взаимодействовать <br> с этой</span>
      <span class="second-banner__heading-fw700">тревожностью?</span>
    </div>

    <p class="second-banner__paragraph decorated-text-block decorated-text-block--n">
      Быть максимально в контакте с реальностью, со своими чувствами- тревогой, страхом, агрессией, позволяя им быть
      на ресурсном уровне, необходимом для поддержания вовлечённости в ситуацию.
    </p>
  </div>
</div>
