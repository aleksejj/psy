<div class="third-banner">
  <div class="third-banner__inner">
    <div class="third-banner__bg-wrapper">
      <img
        class="third-banner__image"
        src="local/templates/psy/images/third-banner-bg.png"
        alt="bar"
        srcset="local/templates/psy/images/third-banner-bg@2x.png 2x"
      />
      <span class="third-banner__bracket svg-icon svg-icon--bracket">
        <svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#bracket"></use></svg>
      </span>
    </div>
    
    <div class="third-banner__phrase">
      Для тех, кто не может в одиночку справиться с тревогой и неопределенностью в эти непростые дни,
      <a href="https://articul.ru/" target="_blank">Articul Club</a> и <a href="https://apcbc.ru/"target="_blank">Ассоциация психоанализа</a> и бизнес–консультирования предлагают
      бесплатную психологическую помощь.
    </div>
    
    <div class="third-banner__phrase2 decorated-text-block decorated-text-block--t">
      Заполните анкету, специалист свяжется с вами и согласует удобное для вас время консультации.
      Консультации проводятся онлайн.
    </div>

    <span class="third-banner__text-w-star">
      <div>
         <span class="svg-icon svg-icon--star">
          <svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#star"></use></svg>
        </span>
      </div>
      <span>
        3 сессии бесплатно (онлайн)
      </span>
    </span>
  </div>
</div>
