<div class="first-banner">
    <div class="first-banner__inner">
        <h1 class="first-banner__heading">
            <?/*<span class="first-banner__heading-fw200">неопределенность</span>
            <br>
            <span class="first-banner__heading-fw400">рождает</span>
            <span class="first-banner__heading-fw700">тревогу</span>*/?>
            неопределенность <br> рождает тревогу
        </h1>
        <p class="first-banner__paragraph decorated-text-block">
            Мы столкнулись с тотальной <span class="decorated-text-block__bold">неопределённостью</span>, у нас нет
            ни малейшего представления, <span class="decorated-text-block__bold">что будет</span> через неделю или месяц.
        </p>
    </div>
</div>
