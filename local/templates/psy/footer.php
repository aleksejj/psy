<footer class="page-footer">
  <div class="page-footer__inner">
      <a href="https://articul.ru" target="_blank" class="svg-icon svg-icon--articul-club">
        <svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#articul-club"></use></svg>
      </a>
  </div>
</footer>
